<?php

namespace Alura\Solid\Model;

interface Assistivel
{
    public function asssistir() : void;
}